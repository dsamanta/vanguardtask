"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var apiOptions = require("../utils/api1");
var request = require("request");
var page2_1 = require("../pages/page2");
describe("Scenario 2: API Validation using Sub Class", function () {
    function APIMethod(done) {
        console.log('calling');
        request('http://api.openweathermap.org/data/2.5/weather', {
            method: 'GET',
            qs: { id: '2172797', APPID: '737eb1ddb3ffaf2157a2f05b7750e7ae' },
            headers: { 'Postman-Token': '2e883d91-f142-47a4-b445-9279cede67ac',
                'cache-control': 'no-cache' }
        }, function (error, response, body) {
            if (error) {
                console.log('error...');
                return console.dir(error);
            }
            console.dir("Body : ******");
            // console.dir(JSON.parse(body));
            console.log("\n\n\nResponse Code ****:" + response.statusCode);
            //expect(response.statusCode).toBe(200)
            JSON.parse(body);
            done();
        });
    }
    var ApiOptions = new apiOptions();
    var api = new page2_1.ApiFunctionalTesting();
    it("Test case 2: API Validation using Sub Class", function (done) {
        APIMethod(done);
    });
});
//# sourceMappingURL=spec2.js.map
var MathematicalSolution = function () {
    this.multiplication = function (a, b) {
        return a * b;
    };
    this.addition = function (a, b) {
        return a + b;
    };
};
module.exports = MathematicalSolution;
//# sourceMappingURL=Nested-Functions.js.map
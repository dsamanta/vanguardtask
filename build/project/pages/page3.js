"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var request = require("request");
var ApiFunctionalTesting = /** @class */ (function () {
    function ApiFunctionalTesting() {
    }
    ApiFunctionalTesting.prototype.APIMethodPage3 = function (done) {
        var options = { method: 'GET',
            url: 'http://api.openweathermap.org/data/2.5/weather',
            qs: { id: '2172797', APPID: '737eb1ddb3ffaf2157a2f05b7750e7ae' },
            headers: { 'Postman-Token': 'ff96ac22-12e9-4743-8240-2ebb624e949d',
                'cache-control': 'no-cache' } };
        function callback(error, response, body) {
            if (error)
                throw new Error(error);
            this.ReturnBody = JSON.parse(response.body);
            console.log(this.ReturnBody);
            done();
        }
        request(options, callback);
        console.log("Result = " + this.ReturnBody);
    };
    return ApiFunctionalTesting;
}());
exports.ApiFunctionalTesting = ApiFunctionalTesting;
//# sourceMappingURL=page3.js.map
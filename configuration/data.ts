
const CONFIG1_ENVIRONMENTS = {
    environment: {},
    Env_Test: {
        baseurl: "",
        uid: ""
    }
};



const CONFIG_ENVIRONMENTS = {
    SIT: {
          base_url:"https://www.vanguardinvestments.com.au/au/portal/homepage.jsp",
          FundCompare_url1:"https://www.vanguardinvestments.com.au/retail/jsp/home.jsp",
          Compare_Products_Url:"https://tool.vanguardinvestments.com.au/mstar/au/fundcompare.htm?site_code=ret##target=fct",

          API: {
              uri:"https://www.vanguardinvestments.com.au/retail/mvc/getNavPriceList.jsonp",
              url2:"",
          },

          configurations: {
            configuration1:"Vanguard",
            
        },

        base4: {
            url1:"",
            url2:"",
        },


},

QAT: {
    base_url:"https://www.vanguardinvestments.com.au/au/portal/homepage.jsp",
    FundCompare_url1:"https://www.vanguardinvestments.com.au/retail/jsp/home.jsp",
    Compare_Products_Url:"https://tool.vanguardinvestments.com.au/mstar/au/fundcompare.htm?site_code=ret##target=fct",

    base2: {
        url1:"",
        url2:"",
    },

    base3: {
      url1:"",
      url2:"",
  },

  base4: {
      url1:"",
      url2:"",
  },


},};


export function getConfig(env: string) {
    (<any>Object).
    assign(CONFIG1_ENVIRONMENTS,CONFIG_ENVIRONMENTS[env]);
    return CONFIG1_ENVIRONMENTS;
}
exports.config = {
     directConnect: true,
     
     specs: [
      // './build/project/specs/VanGuardAPITesting.js',
      // './build/project/specs/VanGuardWebUIAutomation.js',
      //'./build/project/specs/spec1.js',
      './build/project/specs/spec5.js',

    ],


    //  Define Multi-Browser Capabilities
    // multiCapabilities: [
    //   {'browserName': 'chrome',
    //  'chromeOptions': {
    //   'args': [ '--start-maximized', 'disable-infobars','--disable-browser-side-navigation']
    //  }
    // },
    //   {'browserName': 'firefox',
    //  'moz:firefoxOptions': {
    //   'args': [ '--start-maximized', 'disable-infobars','--disable-browser-side-navigation']
    // }
    //  }
    // ],


    capabilities: {
      'browserName': 'chrome',
      'chromeOptions': {
        'args': [ '--headless','--start-maximized', 'disable-infobars','--disable-browser-side-navigation']
      }
    },


    jasmineNodeOpts: {
      showColors: true,
      defaultTimeoutInterval: 30000,
      isVerbose: true
    },

      onPrepare: function () {
        
        let browser = require('protractor').browser;

        return browser.getProcessedConfig().then(function (config) {
              
               // This will read the Data from the Data File
               var browserName = config.capabilities.browserName;
               var environment = require('./build/project/configuration/data');
               browser.params.customConfig = environment.getConfig(browser.params.environment);
               

               // This will display the Stack Trace in CMD
               var SpecReporter = require('jasmine-spec-reporter').SpecReporter;
               

               jasmine.getEnv().addReporter(
                 new SpecReporter({
                   displayStacktrace: 'all',
                   displaySuccessesSummary: true, // display summary of all successes after execution 
                   displayFailuresSummary: true,   // display summary of all failures after execution 
                   displayPendingSummary: false,    // display summary of all pending specs after execution 
                   displaySuccessfulSpec: true,    // display each successful spec 
                   displayFailedSpec: true,        // display each failed spec 
                   displayPendingSpec: false,      // display each pending spec 
                   displaySpecDuration: true,     // display each spec duration 
                   displaySuiteNumber: false,      // display each suite number (hierarchical) 
                  }));


               var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
              
               jasmine.getEnv().addReporter(
                new Jasmine2HtmlReporter({
                  showSummary: true,
                  savePath: './test/reports/',
                  screenshotsFolder: 'images',
                  takeScreenshots: false,
                  takeScreenshotsOnlyOnFailures: false,
                  consolidate: false,
                  consolidateAll: true,
                  cleanDestination: false,
                  showPassed: true,
                  fileName: 'ReportName'+"_"+"Name",
                  showConfiguration: true
                })
              );

              var originalTimeout;

               beforeEach(function() {
                   originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
                   jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
               });
           
               afterEach(function() {
                 jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
               });

            });
        },



    };
var request = require("request");

export class ApiFunctionalTesting {
    
    async APIMethod() {
        console.log('calling');
     
        const result = await request.get({
            url: 'http://api.openweathermap.org/data/2.5/weather',
            qs: { id: '2172797', APPID: '737eb1ddb3ffaf2157a2f05b7750e7ae' },
            headers: 
             { 'Postman-Token': '2e883d91-f142-47a4-b445-9279cede67ac',
               'cache-control': 'no-cache' }
            },  
                (error, response, body) => {
                if(error) {
                    console.log('error...');
                    return console.dir(error);
                }
                console.dir("Body : ******");
                console.dir(JSON.parse(body));
            
                console.log("\n\n\nResponse Code ****:"+response.statusCode)
                //expect(response.statusCode).toBe(200)
             
          });
        console.log(`Result = ${JSON.stringify(result)}`);

    }
}
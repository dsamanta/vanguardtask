import {browser,by,element, ExpectedConditions} from "protractor";
import * as wait from "../utils/wait";
import { exists } from "fs";
import { nextTick } from "q";


export class VanGuardPageAutomation{

    
    // This method will open the Vanguard Compare Product
    async NavigateTo() {
        await browser.waitForAngularEnabled(false);
        await browser.get(browser.params.customConfig.Compare_Products_Url);
    }


    // This method will Navigate to All Funds and select the Fund for comparision
    async Click_AllFunds(F_Name:string,count:number){
            
    //Local Variables: 
        let Breakstatus="start";
        let FundName1:any;
        let NewlyAddedFundName:any;
        

    //Page Elements
        let PageHeadings = element(by.css("h2:nth-child(1)"));
        let PageCompareTable = element(by.css("tbody[id='compareTableResults']"));
        let PageCompareTable_Row = PageCompareTable.all(by.tagName("tr"));
        let PageCompareTable_Column = PageCompareTable_Row.all(by.tagName("td"));

        let Add_Button = element(by.css("button[id='addButton"+count+"']"));
        let AllFundsButton = element(by.css("a[id='fundSelectorOtherFundsTabBtn']"));
        let list = element.all(by.css("ul[class='fundNameSelector']"));
                                      
        let Fund_Lists = list.all(by.tagName("li"));  

        wait.waitForVisibilityOf(PageHeadings);
        
        PageHeadings.isDisplayed().then(function(Heading_IsDisplayed){
        expect(Heading_IsDisplayed).toBe(true);
        });
        
        
        wait.waitToBeClickable(Add_Button);
        Add_Button.click();

        wait.waitForVisibilityOf(AllFundsButton);
        AllFundsButton.click();

        Fund_Lists.count().then(function(Fcount){
        for(var j=0;j<= Fcount-1;j++) {   
            
            if(Breakstatus.match("Break")) {
                break;
            }

            Fund_Lists.get(j).getText().then(function(FundName){

                if(FundName===F_Name){
                   
                   element(by.linkText(FundName)).click();
                   element(by.css("#otherFunds > form > div > table > tbody > tr:nth-child(2) > td:nth-child(1) > input[type=checkbox]")).click();
                   
                   browser.sleep(3000);
                   let button = element(by.css("#addFund"));

                   if(count==0) {    
                   browser.executeScript("arguments[0].scrollIntoView();",button);
                   button.click();

                   } else if(count >= 0) {  
                    let button = element(by.css("#compareFundBtn"));
                    browser.executeScript("arguments[0].scrollIntoView();",button);
                    button.click();
                   }

                   Breakstatus = "Break";
            }  else {
                
        }

    });
            
        }

       
       // Quickly Check if the Funds is added for Comparision : 
       for(var k=1;k<=k+count;k++) {
            PageCompareTable_Column.get(k).getText().then(function(text){
                NewlyAddedFundName = text;
            });
        }
      
       
        console.log( "Newly Added" + NewlyAddedFundName);
        //expect(Fundcount).toBe(count);

           

        });
    }

  
        
    }
 


import {APITesting} from "../utils/api";
import {AutomationTesting} from "../pages/page1";
var ApiOptions = require("../utils/api1");
//var unirest = require('unirest');
var request = require("request");

describe("Scenario 1: Validate the API : Login",function() {
const api:APITesting = new APITesting();
const automation:AutomationTesting = new AutomationTesting();
const apiOptions = new ApiOptions();

it("Test case 1 : API Testing",async(done)=> {

   await request.get({
    url: 'http://api.openweathermap.org/data/2.5/weather',
    qs: { id: '2172797', APPID: '737eb1ddb3ffaf2157a2f05b7750e7ae' },
    headers: 
     { 'Postman-Token': '2e883d91-f142-47a4-b445-9279cede67ac',
       'cache-control': 'no-cache' }
    },
        (error, response, body) => {
      if(error) {
          return console.dir(error);
      }
      console.dir("Body : ******");
      console.dir(JSON.parse(body));

      console.log("\n\n\nResponse Code ****:"+response.statusCode)
     expect(response.statusCode).toBe(200)
     
      done();
  });


});
});


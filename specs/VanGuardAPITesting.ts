import { browser } from "protractor";

var request = require('request');

describe('Vanguard API to validate : ', () => {
    

    it('Validate Net Asset Value (NAV) information on various products', async(done) => {

    console.dir("Starting the API Testing...........");    

        try {
        let uri = browser.params.customConfig.API.uri;

        request.get({
            'uri': browser.params.customConfig.API.uri,

        }, (error, response, body) => {
            if (error) {
                return console.dir(error);
            }

            expect(response.statusCode).toBe(200);
            console.log('\nResponse Code: ' + response.statusCode);
            console.dir("*************************************************************");

            expect(response.headers['content-type']).toBe('application/x-javascript;charset=utf-8');
            console.log('\nResponse Headers: ' + response.headers['content-type']);
            console.dir("*************************************************************");

            let Res = JSON.parse(body);
            console.dir("Total Products Listed : "+Res.length);
            console.dir("*************************************************************");

            for(var i=0;i<=Res.length-1;i++){
                let Product_id = Res[i].portId;
                var NavPriceArray = Res[i].navPriceArray;

                for(var j=0;j<=NavPriceArray.length-1;j++) {
                    
                    //validate the Net Asset Value Structure
                    //*************************************/

                    //Validate the portId to be Defined and Type String
                    expect(Res[i].portId).toBeDefined;
                    expect(typeof Res[i].portId).toEqual("string");
                   
                    //Validate the navPriceArray to be Defined and Type object
                    expect(NavPriceArray[j]).toBeDefined;
                    expect(typeof NavPriceArray[j]).toEqual("object");
                    
                    //Validate the amountChange within navPriceArray to be Defined and Type number
                    expect(NavPriceArray[j].amountChange).toBeDefined;
                    expect(typeof NavPriceArray[j].amountChange).toEqual("number");
                                    
                    //Validate the asOfDate within navPriceArray to be Defined and Type string
                    expect(NavPriceArray[j].asOfDate).toBeDefined;
                    expect(typeof NavPriceArray[j].asOfDate).toEqual("string");
                    

                    //Validate the currency within navPriceArray to be Defined and Type Object
                    expect(NavPriceArray[j].currency).toBeDefined;
                    expect(typeof NavPriceArray[j].currency).toEqual("object");
                   
                    //Validate the currency-currencyCode within navPriceArray to be Defined and Type String
                    expect(NavPriceArray[j].currency.currencyCode).toBeDefined;
                    expect(typeof NavPriceArray[j].currency.currencyCode).toEqual("string");
                    expect(NavPriceArray[j].currency.currencyCode).toEqual("AUD");

                    //Validate the currency-currencyLocation within navPriceArray to be Defined and Type String
                    expect(NavPriceArray[j].currency.currencyLocation).toBeDefined;
                    expect(typeof NavPriceArray[j].currency.currencyLocation).toEqual("string");

                    //Validate the currency-currencySymbol within navPriceArray to be Defined and Type String
                    expect(NavPriceArray[j].currency.currencySymbol).toBeDefined;
                    expect(typeof NavPriceArray[j].currency.currencySymbol).toEqual("string");
                   
                    //Validate the isFinal within navPriceArray to be Defined and Type String
                    expect(NavPriceArray[j].isFinal).toBeDefined;
                    expect(typeof NavPriceArray[j].isFinal).toEqual("boolean");
                    expect(NavPriceArray[j].isFinal).toBe(true);

                    //Validate the measureType within navPriceArray to be Defined and Type Object
                    expect(NavPriceArray[j].measureType).toBeDefined;
                    expect(typeof NavPriceArray[j].measureType).toEqual("object");
                  
                    //Validate the measureType.measureCode within navPriceArray to be Defined and Type String
                    expect(NavPriceArray[j].measureType.measureCode).toBeDefined;
                    expect(typeof NavPriceArray[j].measureType.measureCode).toEqual("string");
                    expect(NavPriceArray[j].measureType.measureCode).toBe("NAV");

                    //Validate the measureType.measureDesc within navPriceArray to be Defined and Type String
                     expect(NavPriceArray[j].measureType.measureDesc).toBeDefined;
                     expect(typeof NavPriceArray[j].measureType.measureDesc).toEqual("string");
                     expect(NavPriceArray[j].measureType.measureDesc).toBe("Net Asset Value");
                    
                    //Validate the percentChange within navPriceArray to be Defined and Type Number
                     expect(NavPriceArray[j].percentChange).toBeDefined;
                     expect(typeof NavPriceArray[j].percentChange).toEqual("number");
                     
                    //Validate the price within navPriceArray to be Defined and Type Number
                    expect(NavPriceArray[j].price).toBeDefined;
                    expect(typeof NavPriceArray[j].price).toEqual("number");   
                    expect(NavPriceArray[j].price).not.toBeNull;
                    
                    //Validate the pricePeriodType within navPriceArray to be Defined and Type Object
                    expect(NavPriceArray[j].pricePeriodType).toBeDefined;
                    expect(typeof NavPriceArray[j].pricePeriodType).toEqual("object");

                    //Validate the pricePeriodType within navPriceArray to be Defined and Type String
                    expect(NavPriceArray[j].pricePeriodType.pricePeriodCode).toBeDefined;
                    expect(typeof NavPriceArray[j].pricePeriodType.pricePeriodCode).toEqual("string");
                    expect(NavPriceArray[j].pricePeriodType.pricePeriodCode).toEqual("DAILY");

                    //Validate the pricePeriodDesc within navPriceArray to be Defined and Type String
                    expect(NavPriceArray[j].pricePeriodDesc).toBeDefined;
                    expect(typeof NavPriceArray[j].pricePeriodType.pricePeriodDesc).toEqual("string");
                    expect(NavPriceArray[j].pricePeriodType.pricePeriodDesc).toEqual("Daily Price");

                    //Validate the priceStatusType within navPriceArray to be Defined and Type Object
                    expect(NavPriceArray[j].priceStatusType).toBeDefined;
                    expect(typeof NavPriceArray[j].priceStatusType).toEqual("object");
                   
                    //Validate the priceStatusCode within navPriceArray to be Defined and Type String
                    expect(NavPriceArray[j].priceStatusType.priceStatusCode).toBeDefined;
                    expect(typeof NavPriceArray[j].priceStatusType.priceStatusCode).toEqual("string");
                    expect(NavPriceArray[j].priceStatusType.priceStatusCode).toEqual("FINAL");
                    
                    //Validate the priceStatusDesc within navPriceArray to be Defined and Type String
                    expect(NavPriceArray[j].priceStatusType.priceStatusDesc).toBeDefined;
                    expect(typeof NavPriceArray[j].priceStatusType.priceStatusDesc).toEqual("string");
                    expect(NavPriceArray[j].priceStatusType.priceStatusDesc).toEqual("Final Price");

                    //Validate the yield within navPriceArray to be Defined and Type String
                    expect(NavPriceArray[j].yield).toBeDefined;
                    expect(typeof NavPriceArray[j].yield).toEqual("object");

                    //Validate the final within navPriceArray to be Defined and Type String
                    expect(NavPriceArray[j].final).toBeDefined;
                    expect(typeof NavPriceArray[j].final).toEqual("boolean");
                    expect(NavPriceArray[j].final).toEqual(true);
                                     
                    console.dir("Product Code : " +Res[i].portId ) ; 
                    console.log("Measure Desc :" +NavPriceArray[j].measureType.measureDesc + ", Price : "+NavPriceArray[j].price);

                    done();

                       
                    console.dir("*************************************************************");  
                }             
            }
           
        });
    
    } catch (exception) {
        done.fail(exception);
    }

    });

});
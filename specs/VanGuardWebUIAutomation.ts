import {VanGuardPageAutomation} from '../pages/FundComparision';

describe("VanGuardPageAutomation",function(){
const VanGuardPage:VanGuardPageAutomation = new VanGuardPageAutomation();

// Enter the VanGuard Funds Name that should be added so that it can be Compared.
let VanguardFundName = [
"Alluvium",
"Baker Steel Capital Managers", 
"Challenger",
"Dalton Nicol Reid"];

it("Add funds to Compare 4 Vanguard Funds",async(done)=> {

    try { 
    console.log("Navigating to Vangurad Fund Comparision Page");
   
    VanGuardPage.NavigateTo();
   
    for(var i=0;i<=VanguardFundName.length-1;i++) {
    console.dir("Fund Name : "+VanguardFundName[i] +" Entered to be Added for Comparison");
    VanGuardPage.Click_AllFunds(VanguardFundName[i],i); 

    }

    done();

    } catch(exception) {
        done.fail(exception); 
    }       
    });
});
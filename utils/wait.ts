import {browser,ElementFinder,ExpectedConditions} from 'protractor';

let TIMEOUT = 6000;

export async function waitForVisibilityOf(element:ElementFinder) {
    return await browser.wait(ExpectedConditions.visibilityOf(element),TIMEOUT);
}

export async function waitForElement(element:ElementFinder){
    return await browser.wait(ExpectedConditions.presenceOf(element),TIMEOUT);
}

export async function waitToBeClickable(element:ElementFinder){
    return await browser.wait(ExpectedConditions.elementToBeClickable(element),TIMEOUT);
}

export async function waitForInvisibilityOf(element:ElementFinder){
    return await browser.wait(ExpectedConditions.invisibilityOf(element),TIMEOUT);
}


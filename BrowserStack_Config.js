var browserstack = require('browserstack-local');

exports.config = {
   'seleniumAddress': 'http://hub-cloud.browserstack.com/wd/hub',

   'capabilities': {
      'os' : 'Windows',
      'os_version' : '10',
      'browserName' : 'Chrome',
      'browser_version' : '62.0',
      'project' : 'VanguardAutomation',
      'build' : 'Build1.1',
      'name' : 'VanguardAutomationFundCompare',
      'browserstack.local' : 'true',
      'browserstack.debug' : 'true',
      'browserstack.networkLogs' : 'true',
      'browserstack.timezone' : 'AEST',
      'browserstack.selenium_version' : '3.5.2',
      'browserstack.user' : 'debashishsamanta1',
      'browserstack.key' : 'HEYL7zur3AW9VSwueTty',

      specs: ['./build/project/specs/spec4.js'],
  },

  // Code to start browserstack local before start of test
  beforeLaunch: function(){
    console.log("Connecting local.....");
    return new Promise(function(resolve, reject){
      exports.bs_local = new browserstack.Local();
      exports.bs_local.start({'key': exports.config.capabilities['browserstack.key'] }, function(error) {
        if (error) return reject(error);
        console.log('Connected. Now testing...');
        resolve();
      });
    });
  },

  // Code to stop browserstack local after end of test
  afterLaunch: function(){
    return new Promise(function(resolve, reject){
      exports.bs_local.stop(resolve);
    });
  }
};